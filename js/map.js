/*name:      Geo-Locastion Site.
description: It is a simple site by which You can search any place near to you.
input:       person input search place, radius and allow location access.
output:      Get location in map of your selected places.
usage:       Use to search any place which you want.
example:     Google search location*/
var map
var loc;
var dis;
var ico;

// Function to show your browser allow to access location and if allow then access your location
function getLocation() {
    if(navigator.geolocation) {
        loc = document.getElementById("search").value;
        dis = document.getElementById("distance").value;
        if(loc == "Bus stop")
            ico = "http://maps.google.com/mapfiles/ms/icons/bus.png";
        else if(loc == "Taxi and car service")
            ico = "http://maps.google.com/mapfiles/ms/icons/cabs.png";
        else if(loc == "metro location")
            ico = "http://maps.google.com/mapfiles/ms/icons/rail.png";
        else if(loc == "Shopping malls")
            ico = "http://maps.google.com/mapfiles/ms/icons/shopping.png";
        else if(loc == "Theaters")
            ico = "http://maps.google.com/mapfiles/ms/icons/movies.png";
        else if(loc == "hospitals")
            ico = "http://maps.google.com/mapfiles/ms/icons/hospitals.png";
        navigator.geolocation.getCurrentPosition(showPosition);
    }
    else
        x.innerHTML = "not supportin";
    }

    //For selecting the your positon in map
    function showPosition(position) {
        var center = {lat : position.coords.latitude, lng : position.coords.longitude}
        map = new google.maps.Map(document.getElementById('map'), {
                        center: center,
                        zoom: 15,
        });
        var you = "http://maps.google.com/mapfiles/ms/icons/blue.png";
        //for marking your location in map
        var marker = new google.maps.Marker({position: center, map: map, title: 'Here You are',icon:you});
        
        //for seaching the near locations
        infowindow = new google.maps.InfoWindow();
        var service = new google.maps.places.PlacesService(map);
        service.nearbySearch({
          location: center,
          radius: +(dis),
          type: ['loc']
        }, callback);
    }

    //for creating the markers in near by seach places
    function callback(results, status) {
        if (status === google.maps.places.PlacesServiceStatus.OK) {
            for (var i = 0; i < results.length; i++) {
               createMarker(results[i]);
            }
        }
    }
    function createMarker(place) {
        var placeLoc = place.geometry.location;
        var marker = new google.maps.Marker({
            map: map,
            position: place.geometry.location,
            icon:ico
        });
    
        google.maps.event.addListener(marker, 'click', function() {
            infowindow.setContent(place.name);
            infowindow.open(map, this);
        });
    }